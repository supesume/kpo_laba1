package com.company;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Language pl = new Language();

        Compiler compiler = pl.choiceLanguage();

        compiler.run("source.txt");

        System.out.println("\nПрограмма выполнена!");

    }
}

