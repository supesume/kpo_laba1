package com.company;

import java.util.Scanner;

public class Language {


    public Compiler choiceLanguage(){

        Scanner in = new Scanner(System.in);
        System.out.print("Ввод ЯП: ");
        String lang = in.nextLine();

        if (lang.equals("Java"))
            return useJava();
        else if (lang.equals("PHP"))
            return usePython();
        else if (lang.equals("JS"))
            return useJS();
        else {
            System.out.println("Язык " + lang + " не поддерживается!");
            return null;
        }

    }
    public Compiler useJava(){
        return new JavaCompiler();
    }

    public Compiler usePython(){
        return new PythonCompiler();
    }

    public Compiler useJS(){
        return new JSCompiler();
    }
}

