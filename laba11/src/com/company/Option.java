package com.company;

class Creator_Compiler{
    private  String language;
    public void setLanguage(String language){
        this.language = language;
    }
    public Compiler createCompiler(){
        if (language.equals("Java"))
            return new JavaCompiler();
        else if (language.equals("Python"))
            return new PythonCompiler();
        else if (language.equals("JS"))
            return new JSCompiler();
        else return null;
    }
}

class Starter_Compiler{


    public void setFile(String path, Compiler compiler){
        compiler.setFile(path);
    }
    public void compile(Compiler compiler){
        compiler.run();
    }

}
