package com.company;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.text.ParseException;

public class HttpClient {
    /**********************************************************************
     класс: HttpClient (HttpClient.java)
     автор: author
     дата: 23.10.2021
     Методы, для работы клиента
     **********************************************************************/

    public static void main(String[] args) {
        try {
            String header = null; // создание строки header(заголовок)
            if (args.length == 0) {
                header = readHeader(System.in);
            } // if
            else {
                FileInputStream fis = new FileInputStream(args[0]); // создание объекта потока ввода файла
                header = readHeader(fis); // присвоение строке header(заголовок)
                fis.close(); // закрытие потока ввода
            } // else
            System.out.println("Заголовок: \n" + header); // вывод заголовка(строки header)
            /* Запрос отправляется на сервер */
            String answer = sendRequest(header); // получение ответа на запрос
            /* Ответ выводится на консоль */
            System.out.println("Ответ от сервера: \n");
            System.out.write(answer.getBytes()); // вывод ответа на запрос
        } // try
        catch (Exception e) { // обработка исключения
            System.err.println(e.getMessage());
            e.getCause().printStackTrace();
        } // catch
    }

    // метод для чтения заголовка, который получает входной поток и возвращает строку с результатом
    public static String readHeader(InputStream strm) throws IOException {
        byte[] buff = new byte[64 * 1024]; // массив для чтения
        int length = strm.read(buff); // создание переменной длины на основе результатов чтения массива
        String res = new String(buff, 0, length); // создание строки с результатом работы метода
        return res; // возврат результата работы метода
    }

    /* метод, который получает заголовок, из которого получает адрес сервера, создаёт сокет,
    отправляет запрос и в результате возвращает ответ от сервера*/
    public static String sendRequest(String httpHeader) throws Exception {
        /* Из http заголовка берется адрес сервера */
        String host = null; // создание переменной host
        int port = 0; // создание переменной port
        try {
            /* присвоение значений для переменной host, с помощью метода getHost(),
            в который передаётся переменная httpHeader*/
            host = getHost(httpHeader);
            /* присвоение значений для переменной port, с помощью метода getPort(),
            в который передаётся переменная host*/
            port = getPort(host);
            /* присвоение значений для переменной host, с помощью метода getHostWithoutPort(),
            в который передаётся переменная host */
            host = getHostWithoutPort(host);
        } // try
        catch (Exception e) { // обработка исключения
            throw new Exception("Не удалось получить адрес сервера.", e);
        }// catch
        Socket socket = null; // создание переменной сокет
        try {
            socket = new Socket(host, port); // создание объекта сокета на основе хоста и порта
            System.out.println("Создан сокет: " + host + " port:" + port); // сообщение с информацией о созданном сокете
            OutputStream out = socket.getOutputStream(); // создание объекта потока вывода
            out.write(httpHeader.getBytes()); // вывод заголовка
            out.flush(); // сбрасывание информации в соответствующий поток
            System.out.println("Заголовок отправлен. \n"); // сообщение с информацией о выполение

            return getAnswer(socket); // получение ответа
        } // try
        catch (Exception e) { // обработка исключения
            throw new Exception("Ошибка при отправке запроса: " + e.getMessage(),e);
        } // catch
        finally {
            socket.close(); // закрытие сокета
        } // finally
    }

    /* метод, который получает сокет и возвращает ответ от сервера*/
    private static String getAnswer(Socket socket) throws Exception {
        String res = null; // создание переменной res
        try {
            InputStreamReader isr = new InputStreamReader(socket
                    .getInputStream()); // создание объекта чтения потока ввода
            BufferedReader bfr = new BufferedReader(isr); // создание объекта чтения данных из потока ввода
            StringBuffer sbf = new StringBuffer(); // создание объекта буферизации строк
            int ch = bfr.read(); // создание переменной ch, на основе данных прочитанных из потока ввода
            while (ch != -1) {
                sbf.append((char) ch);// добавление переменной ch c помощью объекта буферизации строк
                ch = bfr.read();// присвоение переменной ch значения из объекта чтения данных из потока
            } // while
            res = sbf.toString(); // присвоение переменной res, значения объекта чтения данных из потока в строку
        } // try
        catch (Exception e) { // обработка исключения
            throw new Exception("Ошибка при чтении ответа от сервера.", e);
        } // catch
        return res; // возврат результата работы метода
    }

    /* метод, который получает заголовок и возвращает хост */
    private static String getHost(String header) throws ParseException {
        final String host = "Host: "; // строка
        final String normalEnd = "\n"; // строка
        final String msEnd = "\r\n"; // строка

        int s = header.indexOf(host, 0); // создание переменной s по индексу
        if (s < 0) { // если переменная s меньше нуля, то метод возвращает “localhost”
            return "localhost";
        } // if
        s += host.length(); // прибавление к переменной s длины строки хост
        int e = header.indexOf(normalEnd, s); // создание переменной e по индексу из заголовка
        e = (e > 0) ? e : header.indexOf(msEnd, s); // присвоение переменной e значения из заголовка, если е меньше нуля
        if (e < 0) { // если переменная e меньше нуля, тогда обработка исключения
            throw new ParseException("В заголовке запроса не найдено закрывающих символов после пункта Host.",0);
        } // if
        String res = header.substring(s, e).trim(); // получаем строку с результатом работы метода
        return res; // возврат результата работы метода
    }

    /* метод, который получает хост и возвращает порт */
    private static int getPort(String hostWithPort) {
        int port = hostWithPort.indexOf(":", 0); // создание переменной порта из хоста по индексу
        port = (port < 0) ? 80 :
                Integer.parseInt(hostWithPort.substring(port + 1)); // присвоение переменной порт 80, если порт меньше нуля
        return port; // возврат результата работы метода
    }

    /* метод, который получает хост и возвращает хост без порта */
    private static String getHostWithoutPort(String hostWithPort) {
        int portPosition = hostWithPort.indexOf(":", 0); // создание перменной позиции порта по индексу
        if (portPosition < 0) { //если индекс порта меньше нуля, тогда возвращает хост с портом
            return hostWithPort;
        } // catch
        else { //иначе возвращает хост без порта
            return hostWithPort.substring(0, portPosition);
        } // else
    }
}

