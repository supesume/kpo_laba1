package com.company;

import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Client1 {

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        Socket clientSocket = null;
        BufferedReader reader = null;
        BufferedWriter writer = null;

        try {
            clientSocket = new Socket("127.0.0.1", 4444);
        } // try
        catch (IOException ex) {
            System.out.println("Не получается подключиться к порту");
        } // catch

        try {
            reader = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream()));
        } // try
        catch (IOException e) {
            System.out.println("Не удалось получить поток ввода");
        } // catch

        try {
            writer = new BufferedWriter(
                    new OutputStreamWriter(
                            clientSocket.getOutputStream()));
        } // try
        catch (IOException e) {
            System.out.println("Не удалось получить поток вывода");
        } // catch


        System.out.print("Введите запрос: ");
        String request = sc.nextLine();

        // отправка запроса на сервер
        writer.write(request + "\n");
        writer.flush();

        // получение ответа от сервера
        String response = "";
        while ((response = reader.readLine()) != null) {
            System.out.println(response);
        }

        writer.close();
        reader.close();

        clientSocket.close();

    }
}
