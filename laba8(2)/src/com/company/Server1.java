package com.company;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class Server1 {

    public static void main (String[] args) throws InterruptedException {

        ServerSocket serverSocket = null;
        InputStream in = null;
        OutputStream out = null;

        StringBuilder sb = new StringBuilder();
        String response = null;
        int clientCount = 0;
        Random randomizer = new Random();

        ArrayList<String> rybaiList = new ArrayList();
        rybaiList.add("Кто понял жизнь тот больше не спешит,\n" +
                "Смакует каждый миг и наблюдает,\n" +
                "Как спит ребёнок, молится старик,\n" +
                "Как дождь идёт и как снежинки тают.\n" +
                "В обыкновенном видит красоту,\n" +
                "В запутанном простейшее решенье,\n" +
                "Он знает, как осуществить мечту,\n" +
                "Он любит жизнь и верит в воскресенье,\n" +
                "Он понял то, что счастье не в деньгах,\n" +
                "И их количество от горя не спасет,\n" +
                "Но кто живёт с синицею в руках,\n" +
                "Свою жар-птицу точно не найдет\n" +
                "Кто понял жизнь, тот понял суть вещей,\n" +
                "Что совершенней жизни только смерть,\n" +
                "Что знать, не удивляясь, пострашней,\n" +
                "Чем что-нибудь не знать и не уметь.");

        rybaiList.add("Не делай зла — вернется бумерангом,\n" +
                "Не плюй в колодец — будешь воду пить,\n" +
                "Не оскорбляй того, кто ниже рангом,\n" +
                "А вдруг придется, что-нибудь просить.\n" +
                "Не предавай друзей, их не заменишь,\n" +
                "И не теряй любимых — не вернешь,\n" +
                "Не лги себе — со временем проверишь,\n" +
                "Что этой ложью сам себя ты предаёшь.");

        rybaiList.add("Чтоб мудро жизнь прожить, знать надобно немало,\n" +
                "Два важных правила запомни для начала:\n" +
                "Ты лучше голодай, чем что попало есть,\n" +
                "И лучше будь один, чем вместе с кем попало.");

        rybaiList.add("Можно соблазнить мужчину, у которого есть жена.\n" +
                "Можно соблазнить мужчину, у которого есть любовница.\n" +
                "Но нельзя соблазнить мужчину,\n" +
                "У которого есть любимая женщина.");

        rybaiList.add("Дарить себя — не значит продавать.\n" +
                "И рядом спать — не значит переспать.\n" +
                "Не отомстить — не значит все простить.\n" +
                "Не рядом быть — не значит не любить.");

        /* создаём serverSocket, который будет подключен к порту (8000)
            и будет ожидать подключений */
        try {
            serverSocket = new ServerSocket(4444);
            System.out.println("Сервер запущен");
        } // try
        catch (IOException ex) {
            System.out.println("Не получается запустить сервер с данным портом");
        } // catch

        try {
            while (true) {
                /* получаем подключения от клиента, как только клиент подключился,
                у клиентского сокета (clientSocket) выполняется команда создание сокета
                (вызова коструктора)*/
                Socket clientSocket = serverSocket.accept();
                System.out.println("Клиент подключен " + (++clientCount));

                // поток для отправления сообщений через сокет
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(
                                clientSocket.getOutputStream()));

                // поток для получения сообщений через сокет
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(
                                clientSocket.getInputStream()));

                // чтение запроса от клиента
                String request = reader.readLine();
                Thread.sleep(3000);

                if (request.equals("/closeServer")){
                    writer.write("Сервер отключен.");
                    writer.flush();

                    writer.close();
                    reader.close();
                    clientSocket.close();

                    System.out.println("Сервер отключен.");
                } // if

                if (clientCount < 2){
                    sb.append("Запрос №" + clientCount + "\n" + "Длина запроса: " + request.length() + "\n" + "Содержимое запроса: "
                            + request + "\n" + "----------------------------------\n");
                } // if
                else{
                    sb.append("Запрос №" + clientCount + "\n" + "Длина запроса: " + request.length() + "\n" + "Содержимое запроса: "
                            + request + "\n" + "----------------------------------\n"
                            + rybaiList.get((randomizer.nextInt(4 - 0 + 1) + 0)));
                } // else

                // отправка ответа клиенту
                writer.write(sb.toString());
                writer.flush();

                sb.setLength(0);

                // закрываем потоки ввода/вывода и сокет
                writer.close();
                reader.close();
                clientSocket.close();
            } //while
        } // try
        catch (IOException ex) {
            System.out.println("Не получается подключить пользователя.");
        } // catch
    }
}
