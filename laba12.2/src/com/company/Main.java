package com.company;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        //Создание нужного конвертора
        Converter converter = new TxtToXml();
        //Создание класса maker
        Maker maker = new Maker(converter);
        //Конвертация
        maker.startCon("convert.txt","convert.xml");


    }
}
