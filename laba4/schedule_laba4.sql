-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.51 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              11.3.0.6376
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Дамп структуры для таблица schedule_laba4.schedule
CREATE TABLE IF NOT EXISTS `schedule` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `Teacher` int(5) NOT NULL,
  `Subject` int(5) NOT NULL,
  `Day` varchar(15) NOT NULL,
  `classroom` varchar(10) NOT NULL,
  `Quantity` int(3) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `Teacher` (`Teacher`),
  KEY `Subject` (`Subject`),
  CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`Teacher`) REFERENCES `teachers` (`ID`),
  CONSTRAINT `schedule_ibfk_2` FOREIGN KEY (`Subject`) REFERENCES `subjects` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы schedule_laba4.schedule: ~15 rows (приблизительно)
DELETE FROM `schedule`;
INSERT INTO `schedule` (`ID`, `Teacher`, `Subject`, `Day`, `classroom`, `Quantity`) VALUES
	(1, 1, 1, 'Понедельник', 'Д-501', 44),
	(2, 2, 2, 'Понедельник', 'Д-320', 40),
	(3, 3, 3, 'Понедельник', 'A-509', 34),
	(6, 4, 5, 'Среда', 'Д-503', 15),
	(7, 6, 6, 'Среда', 'Д-518', 60),
	(8, 3, 3, 'Среда', 'А-509', 40),
	(9, 2, 2, 'Четверг', 'Д-616', 44),
	(10, 4, 5, 'Четверг', 'Д-521', 35),
	(11, 1, 1, 'Пятница', 'Д-505', 30),
	(12, 3, 3, 'Пятница', 'Д-518', 29),
	(13, 5, 7, 'Пятница', 'Д-417', 15),
	(14, 8, 7, 'Суббота', 'Д-501', 20),
	(15, 7, 8, 'Суббота', 'Д-501', 30),
	(57, 5, 7, 'Вторник', 'Д-417', 10),
	(58, 4, 4, 'Вторник', 'Д-417', 35);

-- Дамп структуры для таблица schedule_laba4.subjects
CREATE TABLE IF NOT EXISTS `subjects` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы schedule_laba4.subjects: ~8 rows (приблизительно)
DELETE FROM `subjects`;
INSERT INTO `subjects` (`ID`, `Name`) VALUES
	(1, 'Архитектура вычислительных сис'),
	(2, 'Психология и педагогика'),
	(3, 'Тестирование ПО'),
	(4, 'Надежность ПО'),
	(5, 'Теория принятия решений'),
	(6, 'Основы управленческой деятельн'),
	(7, 'Разработка защищенных программ'),
	(8, 'Конструирование ПО');

-- Дамп структуры для таблица schedule_laba4.teachers
CREATE TABLE IF NOT EXISTS `teachers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Surname` varchar(20) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Patronymic` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы schedule_laba4.teachers: ~8 rows (приблизительно)
DELETE FROM `teachers`;
INSERT INTO `teachers` (`ID`, `Surname`, `Name`, `Patronymic`) VALUES
	(1, 'Кашковский', 'Виктор', 'Владимирович'),
	(2, 'Сергеева', 'Ирина', 'Альбертовна'),
	(3, 'Лучников', 'Владимир', 'Александрович'),
	(4, 'Ермаков', 'Анатолий', 'Анатольевич'),
	(5, 'Краковский', 'Юрий', 'Мечеславович'),
	(6, 'Кириллова', 'Татьяна', 'Клементьевна'),
	(7, 'Курганская', 'Ольга', 'Викторовна'),
	(8, 'Костенко', 'Мария', 'Сегреевна');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
