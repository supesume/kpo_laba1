package Main;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Класс, организующий подключение к БД
 */
public class DatabaseHandler extends Config{

    Connection dbConnection;

    public Connection getDbConnection()
            throws ClassCastException, SQLException, ClassNotFoundException {

        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

        String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName;

        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);

        return dbConnection;
    }

    /**
     * Функция выводит информацию о преподавателях, работающих в заданный день недели в заданной аудитории
     */
    public void printTask1(String dayOfWeek, String audience){

        try {
            //SQL запрос, считывающий таблицу занятий
            String sql = "SELECT t.Surname, t.Name, t.Patronymic FROM schedule s " +
                    "LEFT JOIN teachers t ON (s.Teacher = t.ID) WHERE s.Day = '" +
                    dayOfWeek + "' AND s.classroom = '" + audience + "'";
            ResultSet res = getDbConnection().prepareStatement(sql).executeQuery();

            while (res.next()) {
                String str = res.getString("Surname") + " " + res.getString("Name") + " " +
                        res.getString("Patronymic");
                System.out.println(str);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

    }

    /**
     * Функция выводит информацию о преподавателях, которые не ведут занятия в заданный день недели
     */
    public void printTask2(String dayOfWeek){

        try {
            String sql = "SELECT t.Surname, t.Name, t.Patronymic FROM teachers t WHERE t.ID NOT IN (SELECT DISTINCT " +
                    "s.Teacher FROM schedule s WHERE s.Day = '" + dayOfWeek + "')";
            ResultSet res = getDbConnection().prepareStatement(sql).executeQuery();

            while (res.next()) {
                String str = res.getString("Surname") + " " + res.getString("Name") + " " +
                        res.getString("Patronymic");
                System.out.println(str);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }


    }

    /**
     * Функция переносит первое занятия заданного дня недели на последнее место
     */
    public void replaceSchedule(String day){

        //SQL запрос, выбирающий из таблицы расписание строку с указанным днем и минимальным ID
        String sql = "SELECT * FROM schedule WHERE Day = '" + day + "' ORDER BY ID ASC LIMIT 1";

        try {

            ResultSet res = getDbConnection().prepareStatement(sql).executeQuery();

            ArrayList<String> data = new ArrayList<>();
            int id = 0;
            //Записываем в массив всю строку
            if (res.next()) {
                id = Integer.parseInt(res.getString("ID"));
                data.add(String.valueOf(res.getInt("Teacher")));
                data.add(String.valueOf(res.getInt("Subject")));
                data.add(res.getString("Day"));
                data.add(res.getString("classroom"));
                data.add(String.valueOf(res.getInt("Quantity")));
            }

            //SQL запрос, удаляющий из таблицы расписание строку c id
            String sqlDelete = "DELETE FROM schedule WHERE ID = " + id;
            //Удаление строки
            getDbConnection().prepareStatement(sqlDelete).executeUpdate();

            String sqlInsert = "INSERT INTO schedule " +
                    "(Teacher, Subject, Day, classroom, Quantity) " +
                    "VALUES ("
                    + Integer.parseInt(data.get(0)) + ","
                    + Integer.parseInt(data.get(1)) + ",'"
                    + data.get(2) + "','"
                    + data.get(3) + "',"
                    + Integer.parseInt(data.get(4)) + ")";

            //Вставка новой строк
            getDbConnection().prepareStatement(sqlInsert).executeUpdate();

        System.out.printf("Перемещена запись: %d %s %s %s %s %s",id,data.get(0),data.get(1),data.get(2),data.get(3),data.get(4));

        }catch (SQLException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

}
