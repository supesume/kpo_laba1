package Main;

public class Main {

    public static void main(String[] args) {

        DatabaseHandler dbHandler = new DatabaseHandler();

        System.out.println("Задание 1:");
        dbHandler.printTask1("Суббота", "Д-501");

        System.out.println("\nЗадание 2:");
        dbHandler.printTask2("Пятница");

        System.out.println("\nЗадание 3:");
        dbHandler.replaceSchedule("Вторник");

    }
}
